#include <iostream>
using namespace std;

int WATERJUG_BFS(int a, int b, int c)
{
  if(c % a == 0)
    return c / a;
  if(c % b == 0)
    return c / b;
  if(c % (a + b) == 0)
    return 2 * c / (a + b);
  int d = a >= b ? a - b : b - a;
  if (c % d != 0)
    return -1;
  else
    return c / d * 2;
}

int main()
{
  cout << WATERJUG_BFS(8, 6, 4) << endl;
  return 0;
}