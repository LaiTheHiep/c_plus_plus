#include <iostream>
#include <vector>
using namespace std;

class Node
{
private:
  long x;
  long m;

public:
  Node(long xi, long mi)
  {
    x = xi;
    m = mi;
  }

  void showInfo()
  {
    cout << "xi: " << x << endl;
    cout << "mi: " << m << endl;
  }

  long time()
  {
    return abs(x);
  }

  long getX() { return x; }
  long getM() { return m; }
};

long timeShip(vector<Node> nodes, int k, bool check)
{
  long result = 0;
  long Sum_mi = 0;
  if (check)
  {
    for (int i = nodes.size() - 1; i >= 0; i--)
    {
      if (Sum_mi + nodes[i].getM() > 0)
      {
        result += nodes[i].time();
      }
      Sum_mi = Sum_mi + nodes[i].getM() - k;
    }
  }
  else
  {
    for (int i = 0; i < nodes.size(); i++)
    {
      if (Sum_mi + nodes[i].getM() > 0)
      {
        result += nodes[i].time();
      }
      Sum_mi = Sum_mi + nodes[i].getM() - k;
    }
  }

  return result * 2;
}

long POSTMAN(int n, int k, Node nodes[])
{
  vector<Node> v1;
  vector<Node> v2;

  // Sap xep
  for (int i = 1; i < n; i++)
  {
    int j = i - 1;
    int m = i;
    while (nodes[j].getX() >= nodes[m].getX())
    {
      Node temp = nodes[j];
      nodes[j] = nodes[m];
      nodes[m] = temp;
      m -= 1;
      j -= 1;
      if (j < 0)
      {
        break;
      }
    }
  }

  // Chia am va duong
  for (int i = 0; i < n; i++)
  {
    if (nodes[i].getX() < 0)
      v1.push_back(nodes[i]);
    else
      v2.push_back(nodes[i]);
  }

  return timeShip(v2, k, true) + timeShip(v1, k, false);
}

int main()
{
  int n = 4;
  int k = 10;
  Node nodes[] = {
      Node(-7, 5),
      Node(-2, 3),
      Node(5, 7),
      Node(9, 5)};

  cout << POSTMAN(n, k, nodes) << endl;

  return 0;
}