#include <iostream>
#include <vector>
using namespace std;

int HIST(int array[], int length)
{
  int Smax = 0;
  for (int i = 0; i < length - 1; i++)
  {
    if (array[i] == 0)
      continue;
    int high = array[i];
    if (Smax < high)
      Smax = high;
    for (int j = i + 1; j < length; j++)
    {
      if (array[j] == 0)
        break;
      if (array[j] <= high)
      {
        high = array[j];
        int s = (j - i + 1) * high;
        if (s > Smax)
        {
          Smax = s;
        }
      }
      else
      {
        int s = (j - i + 1) * high;
        if (s > Smax)
        {
          Smax = s;
        }
      }
    }
  }

  return Smax;
}

int main()
{
  vector<int> results;
  int n;
  cin >> n;
  while (n != 0)
  {
    int array[n];
    for (int i = 0; i < n; i++)
    {
      cin >> array[i];
    }
    results.push_back(HIST(array, n));
    cin >> n;
  }
  
  for (int i = 0; i < results.size(); i++)
  {
    cout << results.at(i) << endl;
  }

  return 0;
}