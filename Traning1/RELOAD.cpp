#include <iostream>
using namespace std;

class InfoUpdate
{
private:
  long stt;
  long number;

public:
  InfoUpdate()
  {
  }
  InfoUpdate(long _stt, long _number)
  {
    stt = _stt;
    number = _number;
  }

  long getSTT() { return stt; }
  long getNumber() { return number; }
};

// return delta result
// result += delta
// A C B, ci <-> C
long update(long *array, long pi, long ci)
{
  if (ci == array[pi])
    return 0;
  long A = array[pi - 1];
  long C = array[pi];
  long B = array[pi + 1];

  array[pi] = ci;

  if (A == C && C == B)
  {
    return 2;
  }
  else if (A == B && C != A)
  {
    if (ci == A)
      return -2;
    else
      return 0;
  }
  else if (A != B && (C == A || C == B))
  {
    if (ci != A && ci != B)
      return 1;
    else
      return 0;
  }
  else
  {
    if (ci == A || ci == B)
      return -1;
    else
      return 0;
  }
}

long caculator(long array[], long n)
{
  long temp = array[0];
  long result = 1;
  for (long i = 1; i < n; i++)
  {
    if (array[i] != temp)
    {
      temp = array[i];
      result += 1;
    }
  }

  return result;
}

void showArray(long array[], long n)
{
  for (long i = 0; i < n; i++)
  {
    cout << array[i] << " == ";
  }
  cout << endl;
}

int main()
{
  long n;
  cin >> n;
  n = n + 2;
  long array[n];
  for (long i = 1; i < n - 1; i++)
  {
    cin >> array[i];
  }
  array[0] = array[1];
  array[n - 1] = array[n - 2];

  long result1 = caculator(array, n);
  long testUpdate;
  cin >> testUpdate;
  InfoUpdate infos[testUpdate];
  for (long i = 0; i < testUpdate; i++)
  {
    long x1, y1;
    cin >> x1 >> y1;
    infos[i] = InfoUpdate(x1, y1);
  }

  for (long i = 0; i < testUpdate; i++)
  {
    result1 += update(array, infos[i].getSTT(), infos[i].getNumber());
    cout << result1 << endl;
  }

  return 0;
}