#include <iostream>
using namespace std;

int findIndexMax(int array[], int start, int end)
{
  int result = start;
  for (int i = start + 1; i < end; i++)
  {
    if (array[result] <= array[i])
      result = i;
  }
  return result;
}

int SIGNAL(int array[], int n, int b)
{
  int indexMax1 = 0;
  int indexMax2 = 0;
  for (int i = 1; i < n; i++)
  {
    if (i > indexMax2)
      indexMax2 = findIndexMax(array, i + 1, n);
    if (array[indexMax2] < b)
      return -1;
    if (array[i - 1] >= array[indexMax1])
      indexMax1 = i - 1;
    if (array[indexMax1] < b)
      continue;
    if (array[indexMax1] - array[i] >= b && array[indexMax2] - array[i] >= b)
      return array[indexMax1] + array[indexMax2] - 2 * array[i];
  }
  return -1;
}

int main()
{
  int n;
  int b;
  cin >> n >> b;
  int array[n];
  for (int i = 0; i < n; i++)
  {
    cin >> array[i];
  }
  
  cout << SIGNAL(array, n, b);
  return 0;
}