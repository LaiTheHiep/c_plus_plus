#include <bits/stdc++.h>
using namespace std;

int s[10000005];
int f[10000005];
int p[10000005];
int n = 0;
bool test[10000005];
int minTime = 999999999;
int maxTime = 0;
int total = 0;

int main()
{
  cin >> n;
  for (int i = 1; i <= n; i++)
  {
    cin >> s[i] >> f[i] >> p[i];
  }
  for (int j = 1; j <= n; j++)
  {
    minTime = s[j];
    maxTime = f[j];
    total = p[j];
    int startTemp = j;
    for (int i = 1; i <= n; i++)
    {
      if (i == startTemp)
      {
        continue;
      }
      if (f[i] <= minTime)
      {
        total += p[i];
        minTime = s[i];
      }
      else if (s[i] >= maxTime)
      {
        total += p[i];
        maxTime = f[i];
      }
      else
      {
        continue;
      }
    }
  }
  cout << total;
  return 0;
}