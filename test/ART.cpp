#include <bits/stdc++.h>
using namespace std;

int N, K;
int t[10000005];
int art[505] = {0};
int result = 0;
int total = 0;
int x;

int main()
{
  cin >> N >> K;
  x = K - 1;
  for (int i = 1; i <= N; i++)
  {
    cin >> t[i];
    total += t[i];
  }
  int avg = total / K;
  int temp = 0;
  for (int i = 1; i <= N; i++)
  {
    cout << i << "-----" << temp << endl;
    if (i == N)
    {
      if (temp + t[i] > result)
      {
        result = temp + t[i];
      }
      break;
    }
    if (temp + t[i] <= avg && N - i > x)
    {
      temp += t[i];
    }
    else
    {
      if (temp > result)
        result = temp;
      temp = 0;
      x--;
    }
  }

  cout << result;
  return 0;
}