#include <bits/stdc++.h>
using namespace std;

int n, m;
int a[1000];
int b[1000];
int mem[1000][1000];
int lcs(int i, int j)
{
  if (i == -1 || j == -1)
    return 0;
  if (mem[i][j] != -1)
    return mem[i][j];
  int res = 0;
  if (a[i] == b[j])
  {
    res = max(res, lcs(i - 1, j));
    res = max(res, lcs(i, j - 1));
    res = max(res, lcs(i - 2, j - 2) + 1);
  }
  else
  {
    res = max(lcs(i - 1, j), lcs(i, j - 1));
  }

  return res;
}

int main()
{
  cin >> n >> m;

  for (int i = 0; i < n; i++)
  {
    cin >> a[i];
  }
  for (int i = 0; i < m; i++)
  {
    cin >> b[i];
  }
  memset(mem, -1, sizeof(mem));
  int k = lcs(n - 1, m - 1);
  cout << k;
  return 0;
}