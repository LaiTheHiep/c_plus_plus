#include <iostream>
#include <string>
using namespace std;

int main()
{
  string s1;
  string s2;
  
  cin >> s1;
  cin >> s2;
  string sum;

  int length = s1.length() >= s2.length() ? s1.length() : s2.length();
  int _numSUM = 0;
  for (int i = length - 1; i >= 0; i--)
  {
    int tempSUM = 0;
    if (i <= s1.length() - 1)
      tempSUM += int(s1[i]) - 48;
    if (i <= s2.length() - 1)
      tempSUM += int(s2[i]) - 48;
    tempSUM += _numSUM;
    if (i == 0)
    {
      if (tempSUM >= 10)
      {
        sum += to_string(tempSUM - 10);
        sum += to_string(1);
      }
      else
      {
        sum += to_string(tempSUM);
      }
      break;
    }
    if (tempSUM >= 10)
    {
      _numSUM = tempSUM / 10;
      sum += to_string(tempSUM % 10);
    }
    else
    {
      sum += to_string(tempSUM);
      _numSUM = 0;
    }
  }

  for (int i = sum.length() - 1; i >= 0; i--)
  {
    cout << sum[i];
  }

  return 0;
}