#include <iostream>
using namespace std;

int maxSub(int array[], int n)
{
  int maxSum = array[0];
  for (int i = 0; i < n; i++)
  {
    int sum = 0;
    for (int j = i; j < n; j++)
    {
      sum += array[j];
      if (sum > maxSum)
        maxSum = sum;
    }
  }
  return maxSum;
}

int maxSub2(int a[], int n)
{
  int smax = a[0];
  int ei = a[0];
  int imax = 0;
  for (int i = 1; i < n; i++)
  {
    int u = ei + a[i];
    int v = a[i];
    if(u > v) ei = u;
    else ei = v;
    if(ei > smax){
      smax = ei;
      imax = i;
    }
  }
  return smax;
}

int main()
{
  int n;
  cin >> n;
  int array[n];
  for (int i = 0; i < n; i++)
  {
    cin >> array[i];
  }

  cout << maxSub(array, n);
  cout << maxSub2(array, n);

  return 0;
}