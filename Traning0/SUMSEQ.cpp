#include <iostream>
#include <math.h>
using namespace std;

int main()
{
  int n;
  cin >> n;
  long arr[n];
  for (int i = 0; i < n; i++)
  {
    cin >> arr[i];
  }

  long d = pow(10, 9) + 7;
  long c = 0;

  for (int i = 0; i < n; i++)
  {
    c = ((c % d) + (arr[i] % d)) % d;
  }

  cout << c;

  return 0;
}