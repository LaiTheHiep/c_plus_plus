#include <iostream>
#include <math.h>
using namespace std;

int main()
{
  long a, b;
  cin >> a >> b;
  long d = pow(10, 9) + 7;
  long a1 = a % d;
  long b1 = b % d;
  long c = (a1 + b1) % d;

  cout << c;

  return 0;
}