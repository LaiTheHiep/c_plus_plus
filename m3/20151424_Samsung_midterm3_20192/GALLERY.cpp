#include <bits/stdc++.h>
#include <iostream>
#include<fstream>
using namespace std;

bool *visited;
int A[100005][100005];
int R[100005];
int counter = 0;

class Graph
{
  int V;
  list<int> *adj;

public:
  Graph(int V);
  int numVertex();
  void addEdge(int v, int w);
  void BFS(int s);
};

Graph ::Graph(int v)
{
  this->V = v;
  adj = new list<int>[this->V];
}

int Graph ::numVertex()
{
  return V;
}

void Graph ::addEdge(int u, int v)
{
  adj[u].push_back(v);
}

void Graph ::BFS(int s)
{
  queue<int> q;
  visited[s] = true;
  q.push(s);

  list<int>::iterator i;
  while (!q.empty())
  {
    s = q.front();
    // cout << s << " ";
    R[counter] = s;
    counter = counter + 1;
    q.pop();

    for (i = adj[s].begin(); i != adj[s].end(); ++i)
    {
      int v = *i;
      if (!visited[v])
      {
        visited[v] = true;
        q.push(v);
      }
    }
  }
}

int main()
{
  freopen("GALLERY.inp", "r", stdin);
  freopen("GALLERY.out", "w", stdout);
  int n, m;
  cin >> n >> m;
  Graph g(n);

  int s, t, c;
  for (int i = 0; i < m; i++)
  {
    cin >> s >> t >> c;
    g.addEdge(s, t);
    A[s][t] = A[t][s] = c;
  }

  for (int i = 0; i < n; i++)
  {
    A[i][i + 1] = A[i + 1][i] = 0;
  }

  g.BFS(0);
  int kq = 0;
  for (int i = 0; i < n; i++)
  {
    kq += A[R[i]][R[i + 1]];
  }
  cout << kq;

  return 0;
}
