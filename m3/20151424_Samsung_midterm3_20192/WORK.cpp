#include <iostream>
#include <bits/stdc++.h>
#include<fstream>
using namespace std;

int n;
int d[1000005];
int p[1000005];
int stt[1000005];
bool visited[1000005] = {false};

void swap(int *xp, int *yp)
{
  int temp = *xp;
  *xp = *yp;
  *yp = temp;
}

void selectionSort(int arr[], int _d[], int _stt[], int n)
{
  int i, j, max_idx;

  for (i = 0; i < n - 1; i++)
  {
    max_idx = i;
    for (j = i + 1; j < n; j++)
      if (arr[j] > arr[max_idx])
        max_idx = j;
    swap(&arr[max_idx], &arr[i]);
    swap(&_d[max_idx], &_d[i]);
    swap(&_stt[max_idx], &_stt[i]);
  }
}

int main()
{
  freopen("WORK.inp", "r", stdin);
  freopen("WORK.out", "w", stdout);
  ios_base::sync_with_stdio(0);
  cin >> n;
  for (int i = 0; i < n; i++)
  {
    cin >> d[i] >> p[i];
    stt[i] = i;
  }
  selectionSort(d, p, stt, n);

  return 0;
}