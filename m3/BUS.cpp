#include <bits/stdc++.h>
#include <iostream>
#include<fstream>
using namespace std;

bool *visited;
int A[2005][2005] = {10000};
int R[1005];
int counter = 0;
class Graph
{
  int V;
  list<int> *adj;

public:
  Graph(int V);
  int numVertex();
  void addEdge(int v, int w);
  void BFS(int s);
};

Graph ::Graph(int v)
{
  this->V = v;
  adj = new list<int>[this->V];
}

int Graph ::numVertex()
{
  return V;
}

void Graph ::addEdge(int u, int v)
{
  adj[u].push_back(v);
}

void Graph ::BFS(int s)
{
  queue<int> q;
  visited[s] = true;
  q.push(s);

  list<int>::iterator i;
  while (!q.empty())
  {
    s = q.front();
    R[counter] = s;
    counter = counter + 1;
    q.pop();

    for (i = adj[s].begin(); i != adj[s].end(); ++i)
    {
      int v = *i;
      if (!visited[v])
      {
        visited[v] = true;
        q.push(v);
      }
    }
  }
}

int main()
{
  freopen("BUS.inp", "r", stdin);
  freopen("BUS.out", "w", stdout);
  int N, C, M;
  cin >> N >> C >> M;
  Graph g(N);

  int u, v, t;
  for (int i = 0; i < C; i++)
  {
    cin >> u >> v;
    g.addEdge(u, v);
    A[u - 1][v - 1] = 0;
  }

  for (int i = 0; i < M; i++)
  {
    cin >> u >> v >> t;
    if (A[u - 1][v - 1] > t)
      A[u - 1][v - 1] = t;
  }

  // int numV = g.numVertex();
  visited = new bool[N];
  for (int i = 0; i < N; i++)
  {
    visited[i] = false;
  }

  g.BFS(1);
  int kq = 0;
  for (int i = 0; i < N; i++)
  {
    kq += A[R[i]][R[i + 1]];
  }
  cout << kq;

  return 0;
}
