#include <iostream>
#include <bits/stdc++.h>
using namespace std;

int const V = 4;
int colorArr[100];
bool isBipartite(int A[][V], int src)
{
  for (int i = 0; i < V; ++i)
  {
    colorArr[i] = -1;
  }
  colorArr[src] = 1;

  queue<int> q;
  q.push(src);

  while (!q.empty())
  {
    int u = q.front();
    q.pop();

    if (A[u][u] == 1)
      return false;
    for (int v = 0; v < v < V; v++)
    {
      if (A[u][v] && colorArr[v] == -1)
      {
        colorArr[v] = 1 - colorArr[u];
        q.push(v);
      }
      else
      {
        if (A[u][v] && colorArr[v] == colorArr[u])
          return false;
      }
    }
  }

  return true;
}

int main()
{
  int A[V][V] = {
      {0, 1, 0, 1},
      {1, 0, 1, 0},
      {0, 1, 0, 1},
      {1, 0, 1, 0}};

  isBipartite(A, 0) ? cout << "Yes" : cout << "No";

  return 0;
}