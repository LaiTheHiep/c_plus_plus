#include <iostream>
#include <bits/stdc++.h>
using namespace std;

bool *visited;
class Graph
{
  int V;
  list<int> *adj;

public:
  Graph(int V);
  int numVertex();
  void addEdge(int v, int w);
  void BFS(int s);
  void DFS(int s);
  void DFS_topo(int s, stack<int> &Stack);
  void topoSort();
  void xoaDanDinh_topo();
};

Graph ::Graph(int v)
{
  this->V = v;
  adj = new list<int>[this->V];
}

int Graph ::numVertex()
{
  return V;
}

void Graph ::addEdge(int u, int v)
{
  adj[u].push_back(v);
}

void Graph ::BFS(int s)
{
  queue<int> q;
  visited[s] = true;
  q.push(s);

  list<int>::iterator i;
  while (!q.empty())
  {
    s = q.front();
    cout << s << " ";
    q.pop();

    for (i = adj[s].begin(); i != adj[s].end(); ++i)
    {
      int v = *i;
      if (!visited[v])
      {
        visited[v] = true;
        q.push(v);
      }
    }
  }
}

void Graph ::DFS(int s)
{
  visited[s] = true;
  cout << s << " ";
  list<int>::iterator i;
  for (i = adj[s].begin(); i != adj[s].end(); ++i)
  {
    int v = *i;
    if (!visited[v])
    {
      visited[v] = true;
      DFS(v);
    }
  }
}

void Graph ::DFS_topo(int s, stack<int> &Stack)
{
  visited[s] = true;
  list<int>::iterator i;
  for (i = adj[s].begin(); i != adj[s].end(); ++i)
  {
    int v = *i;
    if (!visited[v])
      DFS_topo(v, Stack);
  }
  Stack.push(s);
}

void Graph ::topoSort()
{
  for (int s = 0; s < V; s++)
  {
    visited[s] = false;
  }
  stack<int> Stack;

  for (int s = 0; s < V; s++)
  {
    if (!visited[s])
      DFS_topo(s, Stack);
  }
  while (!Stack.empty())
  {
    cout << Stack.top() << " ";
    Stack.pop();
  }
}

void Graph ::xoaDanDinh_topo()
{
  vector<int> in_degree(V, 0);
  for (int v = 0; v < V; v++)
  {
    list<int>::iterator i;
    for (i = adj[v].begin(); i != adj[v].end(); i++)
    {
      in_degree[*i]++;
    }
  }
  queue<int> q;
  for (int v = 0; v < V; v++)
  {
    if (in_degree[v] == 0)
      q.push(v);
  }
  int num = 0;
  vector<int> top_order;
  while (!q.empty())
  {
    int v = q.front();
    q.pop();
    num++;
    top_order.push_back(v);
    list<int>::iterator it;
    for (it = adj[v].begin(); it != adj[v].end(); it++)
    {
      int u = *it;
      in_degree[u]--;
      if (in_degree[u] == 0)
        q.push(u);
    }
  }

  if (num != V)
  {
    cout << "Do thi co chu trinh\n";
    return;
  }
  for (int i = 0; i < top_order.size(); i++)
  {
    cout << top_order[i] << " ";
  }
}

int main()
{
  Graph g(5);
  g.addEdge(4, 2);
  g.addEdge(3, 5);
  g.addEdge(2, 3);
  g.addEdge(3, 1);
  g.addEdge(1, 2);
  g.addEdge(4, 2);
  g.addEdge(2, 1);

  int numV = g.numVertex();
  visited = new bool[numV];
  for (int i = 0; i < numV; i++)
  {
    visited[i] = false;
  }

  cout << "BFS(1): ";
  g.BFS(1);
  cout << endl;

  // cout << "DFS(1): ";
  // for (int i = 0; i < numV; i++)
  // {
  //   visited[i] = false;
  // }
  // g.DFS(1);

  // cout << endl;
  // cout << "Thu tu topo: ";
  // g.topoSort();

  // // case 2
  // Graph g2(10);
  // g2.addEdge(9, 6);
  // g2.addEdge(0, 6);
  // g2.addEdge(0, 4);
  // g2.addEdge(0, 1);
  // g2.addEdge(1, 2);
  // g2.addEdge(4, 5);
  // g2.addEdge(6, 3);
  // g2.addEdge(3, 4);
  // g2.addEdge(3, 8);
  // g2.addEdge(2, 5);
  // g2.addEdge(2, 7);
  // g2.addEdge(7, 8);
  // cout << endl;
  // g2.xoaDanDinh_topo();

  return 0;
}