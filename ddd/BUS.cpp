#include <bits/stdc++.h>
using namespace std;

bool *visited;
int trongso[2005][2005] = {10005};
int path[1005];
int dem = 0;

class Graph
{
  int V;
  list<int> *adj;

public:
  Graph(int V);
  void addEdge(int v, int w);
  void DFS(int s);
};

Graph ::Graph(int v)
{
  this->V = v;
  adj = new list<int>[this->V];
}

void Graph ::addEdge(int u, int v)
{
  adj[u].push_back(v);
}

void Graph ::DFS(int s)
{
  visited[s] = true;
  path[dem] = s;
  list<int>::iterator i;
  for (i = adj[s].begin(); i != adj[s].end(); ++i)
  {
    int v = *i;
    if (!visited[v])
    {
      visited[v] = true;
      DFS(v);
    }
  }
}

void print(int n)
{
  int r = 0;
  for (int i = 0; i < n; i++)
  {
    r += trongso[path[i]][path[i + 1]];
  }
  cout << r;
}

int main()
{
  int n, c, m;
  Graph g(n);
  for (int i = 0; i < c; i++)
  {
    int u, v;
    cin >> u >> v;
    g.addEdge(u, v);
    trongso[u][v] = 0;
  }

  for (int i = 0; i < m; i++)
  {
    int u, v, t;
    cin >> u >> v >> t;
    if (trongso[u][v] > t)
      trongso[u - 1][v - 1] = t;
  }

  for (int i = 0; i < n; i++)
  {
    visited[i] = false;
  }
  g.DFS(1);
  print(n);

  return 0;
}