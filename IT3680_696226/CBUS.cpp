#include <bits/stdc++.h>
using namespace std;
#define NMAX 50
#define INFINITY_MAX 1e9

int n, m, c_min;
bool visited[NMAX];
int dem, seat;
int x[NMAX];
int f, fopt, g;
int c[NMAX][NMAX];

int UCV(int i)
{
  if(i <= n)
    return !visited[i] && dem < seat;
  else
    return visited[i - n] && !visited[i];
}

void Try(int k)
{
  for (int i = 1; i <= 2 * n; i++)
  {
    if(UCV(i))
    {
      if(i <= n)
        dem++;
      else
        dem--;
      x[k] = i, visited[i] = true;
      f = f + c[x[k - 1]][i];
      if(k == 2 * n)
      {
        int temp = f + c[x[2 * n]][0];
        if(temp < fopt)
          fopt = temp;
      }
      else
      {
        g = f + (2 * n + 1 - k) * c_min;
        if(g < fopt)
          Try(k + 1);
      }
      visited[i] = false;
      f = f - c[x[k - 1]][i];
      if(i <= n)
        dem--;
      else
        dem++;
    }
  }
}

void BranchAndBound()
{
  c_min = INFINITY_MAX;
  cin >> n >> seat;
  int N = 2 * n;

  for (int i = 0; i <= N; i++)
  {
    for (int j = 0; j <= N; j++)
    {
      cin >> c[i][j];
      if(i != j)
      {
        if(c_min > c[i][j])
          c_min = c[i][j];
      }
    }
  }
  
  fopt = INFINITY_MAX;
  f = 0;
  dem = 0;
  x[0] = 0; visited[0] = true;
  Try(1);
  cout << fopt;
}

int main()
{
  BranchAndBound();

  return 0;
}