#include <bits/stdc++.h>
using namespace std;

const int N = 1005;

int n, T, D;
int t[N];
int a[N];
int dp[N][N];

int main()
{
	cin >> n >> T >> D;
	for (int i = 1; i <= n; i++)
	{
		cin >> a[i];
	}
	for (int i = 1; i <= n; i++)
	{
		cin >> t[i];
	}

	int ans = 0;
	for (int i = 1; i <= n; i++)
	{
		for (int j = t[i]; j <= T; j++)
		{
			for (int s = i - 1; s >= max(0, i - D); s--)
			{
				dp[i][j] = max(dp[i][j], dp[s][j - t[i]] + a[i]);
			}
			ans = max(ans, dp[i][j]);
		}
	}
	cout << ans;
	return 0;
}