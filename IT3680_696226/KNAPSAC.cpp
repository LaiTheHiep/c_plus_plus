#include <bits/stdc++.h>
using namespace std;
#define NMAX 32
//#define INFINITY -1e9

struct item
{
	int a;
	int c;
} dovat[NMAX];

int n, b;
int fopt, fk, bk;
int x[NMAX];

void input()
{
	cin >> n >> b;
	for(int i = 1; i <= n; i++)
	{
		cin >> dovat[i].a >> dovat[i].c;
	}
}

/*
int BranchAndBound()
{
	fopt = INFINITY;
	bk = b;
	fk = 0;
	Try(1);
	return fopt;
}
*/

bool cmp(item x, item y)
{
	return ((float)x.c) / x.a > ((float)y.c) / y.a;
}

void Try(int k)
{
	int j, t;
	if(bk >= dovat[k].a)
		t = 1;
	else
		t = 0;

	for(j = t; j >= 0; j--)
	{
		x[k] = j;
		bk = bk - dovat[k].a * x[k];
		fk = fk + dovat[k].c * x[k];
		if(k == n)
		{
			if(fk > fopt)
				fopt = fk;
		}
		else
		{
			float  g = fk + (float)dovat[k + 1].c * (bk) / dovat[k + 1].a;
			if(g > fopt)
				Try(k + 1);
		}

		bk = bk + dovat[k].a * x[k];
		fk = fk - dovat[k].c * x[k];
	}
}

void branch_and_bound()
{
	fopt = 0;
	sort(dovat + 1, dovat + n + 1, cmp);
	bk = b;
	fk = 0;
	Try(1);
	cout << fopt;
}

int main()
{
	input();
	branch_and_bound();
	return 0;
}
