#include <bits/stdc++.h>
using namespace std;
const int N = 1005;
int n, k1, k2;
int dp[2][N];

int main()
{
  cin >> n >> k1 >> k2;

  dp[0][0] = dp[1][0] = 1;
  for (int i = 1; i <= n; i++)
  {
    dp[0][i] = dp[1][i - 1];
    for (int d = i - k1; d >= max(0, i - k2); d--)
    {
      dp[1][i] += dp[0][d];
    }
  }
  cout << dp[0][n] + dp[1][n];

  return 0;
}