#include <bits/stdc++.h>
using namespace std;

int main()
{
  int n, count;
  pair<int, int> deparment[200001];
  priority_queue<int, vector<int>, greater<int>> Queue;
  cin >> n;
  for (int i = 1; i <= n; i++)
  {
    cin >> deparment[i].first >> deparment[i].second;
  }
  sort(deparment + 1, deparment + n + 1);
  count = 1;

  Queue.push(deparment[1].second);
  for (int i = 2; i <= n; i++)
  {
    if (deparment[i].first >= Queue.top())
    {
      Queue.push(deparment[i].second);
      Queue.pop();
    }
    else
    {
      count++;
      Queue.push(deparment[i].second);
    }
  }
  cout << count;

  return 0;
}