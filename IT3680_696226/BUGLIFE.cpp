#include <bits/stdc++.h>

using namespace std;

const int N = 2005;

int n, m;
int mark[N];
vector<int> a[N];

void DFS(int u)
{
  // Xét tất cả các đỉnh v kề với u
  for(int i = 0; i < a[u].size(); i++) {
    int v = a[u][i];
    if(!mark[v]) {
      // v sẽ có đánh dấu ngược với u
      mark[v] = -mark[u];
      DFS(v);
    }
  }
}

int main()
{
  ios_base::sync_with_stdio(0);
  int numT;
  cin >> numT;
  for(int tc = 1; tc <= numT; tc++) {
    cin >> n >> m;

    memset(mark, 0, sizeof(mark));
    for(int i = 1; i <= n; i++) {
      a[i].clear();
    }
    int u, v;
    for(int i = 1; i <= m; i++) {
      cin >> u >> v;
      a[u].push_back(v);
      a[v].push_back(u);
    }

    for(int i = 1; i <= n; i++) {
      // Với mỗi thành phần liên thông, ta sẽ DFS 1 lần
      if(!mark[i]) {
        // Khởi tạo giá trị cho con bọ đầu tiên
        mark[i] = 1;
        DFS(i);
      }
    }

    bool ans = true;
    // Xét tất cả các cạnh (u, v)
    // Nếu u và v cùng loại thì không thỏa mãn
    for(int i = 1; i <= n; i++) {
      for(int j = 0; j < a[i].size(); j++) {
        if(mark[i] == mark[a[i][j]]) {
          ans = false;
        }
      }
    }

    cout << "Scenario #" << tc << ":\n";
    if(ans) {
      cout << "No suspicious bugs found!\n";
    } else {
      cout << "Suspicious bugs found!\n";
    }
  }
  return 0;
}
