#include <bits/stdc++.h>
using namespace std;
#define NMAX 50
#define INFINITY_MAX 1e9

int c_min, fopt, f, g;
int n, N;
int c[NMAX][NMAX];
int x[NMAX];
bool visited[NMAX];

void Try(int k)
{
  for (int v = 1; v <= n; v++)
  {
    if (!visited[v])
    {
      x[k] = v;
      visited[v] = true;
      if (k == 1)
        f = c[0][v] + c[v][v + n];
      else
        f = f + c[x[k - 1] + n][v] + c[v][v + n];

      if (k == n)
      {
        int temp = f + c[v + n][0];
        if (temp < fopt)
          fopt = temp;
      }
      else
      {
        g = f + (2 * (n - k) + 1) * c_min;
        if (g < fopt)
          Try(k + 1);
      }

      visited[v] = 0;
      if (k == 1)
        f = 0;
      else
        f = f - c[x[k - 1] + n][v] - c[v][v + n];
    }
  }
}

void BranchAndBound()
{
  c_min = INFINITY_MAX;
  cin >> n;
  int N = 2 * n;
  for (int i = 0; i <= N; i++)
  {
    for (int j = 0; j <= N; j++)
    {
      cin >> c[i][j];
      if (i != j)
      {
        if (c_min > c[i][j])
          c_min = c[i][j];
      }
    }
  }

  for (int i = 0; i <= n; i++)
  {
    visited[i] = false;
  }

  fopt = INFINITY_MAX;
  f = 0;
  x[0] = 0;
  visited[0] = true;
  Try(1);
  cout << fopt;
}

int main()
{
  BranchAndBound();

  return 0;
}