#include <iostream>
using namespace std;

int WATERJUG_BFS(int a1, int b1, int c)
{
  int a, b;
  if(a1 < b1){
    a = a1;
    b = b1;
  }
  else{
    a = b1;
    b = a1;
  }
  for(int i = 0; i < a + b + c; i++){
    if((c - b*i) % a == 0)
      return i + abs((c - b*i) / a);
  }
  return -1;
}


int main()
{
	int a, b, c;
	cin >> a >> b >> c;
	cout << WATERJUG_BFS(a, b, c);
  //int n;
	//cin >> n;
	//int arr[n * 3];
	//for(int i=0; i < 3 * n; i++){
		//cin >> arr[i];
	//}

	//for(int i=0; i < 3 * n; i += 3){
		//cout << WATERJUG_BFS(arr[i], arr[i + 1], arr[i + 2]) << endl; 
	//}

  return 0;
}
