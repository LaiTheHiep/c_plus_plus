#include <bits/stdc++.h>
using namespace std;
#define NMAX 32

int n, m;
int conflict[NMAX][NMAX];
int canTeach[NMAX][NMAX];
int assigned[NMAX];
int load[NMAX] = {0};
int fopt;
int MAXLOAD[NMAX] = {0};

void input()
{
  cin >> n >> m;
  int i, k, j;
  for (i = 1; i <= n; i++)
  {
    cin >> k;
    while (k--)
    {
      cin >> j;
      canTeach[i][j] = 1;
    }
  }
  cin >> k;
  while (k--)
  {
    cin >> i >> j;
    conflict[i][j] = 1;
    conflict[j][i] = 1;
  }
}

bool UCV(int t, int k)
{
  if (canTeach[t][k] != 1)
    return false;
  for (int i = 1; i <= k - 1; i++)
  {
    if (assigned[i] == t && conflict[i][k] == 1)
      return false;
  }

  return true;
}

void Try(int k)
{
  for (int t = 1; t <= n; t++)
  {
    if (UCV(t, k))
    {
      assigned[k] = t;
      load[t]++;
      MAXLOAD[k] = max(MAXLOAD[k - 1], load[t]);
      if (k == m)
      {
        fopt = min(fopt, MAXLOAD[m]);
      }
      else
      {
        if (MAXLOAD[k] < fopt)
          Try(k + 1);
      }
      load[t]--;
    }
  }
}

void BranchAndBound()
{
  fopt = INT_MAX;
  Try(1);
  if (fopt == INT_MAX)
    cout << "-1";
  else
    cout << fopt;
}

int main()
{
  input();
  BranchAndBound();

  return 0;
}