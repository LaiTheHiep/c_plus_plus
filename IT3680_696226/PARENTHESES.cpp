#include<bits/stdc++.h> 
using namespace std;

int PARENTHESES(string s){
	vector<char> a;
	a.clear();
	for(int i = 0; i < s.size(); i++){
		if(s[i] == '(' || s[i] == '[' || s[i] == '{'){
			a.push_back(s[i]);
		}else if(s[i] == ')'){
			if(a.back() == '(') a.pop_back();
			else return 0;
		}else if(s[i] == ']'){
			if(a.back() == '[') a.pop_back();
			else return 0;
		}else if(s[i] == '}'){
			if(a.back() == '{') a.pop_back();
			else return 0;
		}
	}
	if(a.size() != 0) return 0;
	return 1;
}

int main(){
	int n;
	cin >> n;
	string s[n];
	for(int i = 0; i < n; i++){
		cin >> s[i];
	}
	for(int i = 0; i < n; i++){
		cout << PARENTHESES(s[i]) << endl;
	}
	return 0;
}
