#include <iostream>
using namespace std;
#define MAX 21
#define INFINITY 1e9
 
int n;
int c[MAX][MAX];
int x[MAX];
bool visited[MAX];
int f, fopt, xopt;
int cmin;
 
void input(){
  int m;
  cin >> n >> m;
  cmin = INFINITY;
  for(int i = 0; i <= n; i++){
    for(int j = 0; j <= n; j++){
      if(i == j)
        c[i][j] = 0;
      else
        c[i][j] = INFINITY;
    }
  }
  for(int k = 1; k <= m; k++){
    int i, j;
    cin >> i >> j;
    cin >> c[i][j];
    if(i != j && c[i][j] < cmin)
      cmin = c[i][j];
  }
}
 
void Try(int k){
  for(int v = 1; v <= n; v++){
    if(visited[v] == false && c[x[k - 1]][v] != INFINITY){
      x[k] = v; visited[v] = true;
      f = f + c[x[k - 1]][x[k]];
      if(k == n){
        int temp = f + c[x[n]][x[1]];
        if(temp < fopt)
          fopt = temp;
      }
      else{
        int g = f + (n - k + 1) * cmin;
        if(g< fopt)
          Try(k + 1);
      }
      
      f = f - c[x[k - 1]][x[k]];
      visited[v] = false;
    }
  }
}
 
int BranchAndBound(){
  fopt = INFINITY;
  for(int v = 1; v <= n; v++){
    visited[v] = false;
  }
  f = 0;
  x[1] = 1;
  visited[x[1]] = true;
  Try(2);
  cout << fopt;
}
 
 
int main(){
  input();
  BranchAndBound();
  return 0;
}
