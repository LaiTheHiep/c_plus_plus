#include <bits/stdc++.h>

using namespace std;

const int N = 5005;
const int INF = 1000000000;

int n, k;
int c[N], d[N], dist[N], mark[N];
vector<int> a[N];
vector<pair<int, int> > adj[N];

void BFS(int u)
{
  queue<int> q;
  memset(mark, 0, sizeof(mark));
  // Khởi tạo queue
  dist[u] = 0; // Khoảng cách đến đỉnh đang xét
  mark[u] = 1; // Đánh dấu đã được thăm hay chưa
  q.push(u);

  // Chạy cho đến khi queue rỗng
  while(!q.empty()) {
    // Lấy ra phần tử đầu tiên
    int x = q.front();
    q.pop();

    // Xét các đỉnh kề với x
    for(int i = 0; i < a[x].size(); i++) {
      // Lấy đỉnh đầu tiên trong queue
      int v = a[x][i];

      // Nếu v chưa được thăm
      if(!mark[v]) {
        mark[v] = 1;
        dist[v] = dist[x] + 1;
        q.push(v);

        // Đường đi ngắn nhất đến v thỏa mãn điều kiện
        if(dist[v] <= d[u]) {
          adj[u].push_back(make_pair(v, c[u]));
        }
      }
    }
  }
}

// Thuật toán Dijkstra
void ShortestPath(int u)
{
  // Priority queue mặc định là heap max
  // Do đó, để lấy phần tử nhỏ nhất thì ta sẽ đẩy vào giá trị âm
  priority_queue<pair<int, int> > pq;

  // Khởi tạo mảng khoảng cách bằng 1 số rất lớn
  for(int i = 1; i <= n; i++) {
    dist[i] = INF;
  }
  // Khởi tạo giá trị cho đỉnh bắt đầu
  dist[u] = 0;
  pq.push(make_pair(-dist[u], u));

  // Chạy cho đến khi priority queue rỗng
  while(!pq.empty()) {
    // Lấy ra phần tử có khoảng cách nhỏ nhất
    pair<int, int> t = pq.top();
    pq.pop(); // Bỏ phần tử đó ra khỏi priority queue

    int uu = t.second; // Đỉnh đang xét
    int ww = -t.first; // Giá trị của đỉnh hiện tại
    // Nếu giá trị lấy ra khác với giá trị hiện tại thì bỏ qua
    if(dist[uu] != ww) continue;

    // Tính độ dài đường đi dựa vào đỉnh hiện tại
    for(int i = 0; i < adj[uu].size(); i++) {
      int vv = adj[uu][i].first;
      int l = adj[uu][i].second;

      //Nếu đường đi mới tốt hơn thì đẩy giá trị mới đó vào priority queue
      if(dist[vv] > dist[uu] + l) {
        // Cập nhật lại khoảng cách
        dist[vv] = dist[uu] + l;
        pq.push(make_pair(-dist[vv], vv));
      }
    }
  }
}

int main()
{
  cin >> n >> k;
  for(int i = 1; i <= n; i++) {
    cin >> c[i] >> d[i];
  }
  int u, v;
  for(int i = 1; i <= k; i++) {
    cin >> u >> v;
    a[u].push_back(v);
    a[v].push_back(u);
  }

  // Xây dựng đồ thị mới từ đề bài
  for(int i = 1; i <= n; i++) {
    BFS(i);
  }

// In ra đồ thị mới được xây dựng
 for(int i = 1; i <= n; i++) {
   for(int j = 0; j < adj[i].size(); j++) {
     cout << i << " " << adj[i][j].first << " " << adj[i][j].second << endl;
   }
 }

  // Tìm đường đi ngắn nhất từ 1 đến các đỉnh còn lại
  // ShortestPath(1);

  // cout << dist[n];
  return 0;
}
